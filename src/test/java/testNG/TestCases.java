package testNG;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import helper.DriverType;
import config.ConfigFileReader;
import helper.DashboardHelper;

public class TestCases {

	WebDriver driver;

	@BeforeTest
	void hiturl() {
		driver = DriverType.typeofdriver("chrome");
		String URL = ConfigFileReader.getConfigValue("url");
		driver.get(URL);
	}

	@Test
	public void test1() {
		DashboardHelper obj = new DashboardHelper(driver);
		obj.setDetails("Delhi", "Pune", "Wed Jan 29 2020");
		obj.searchFlights();
	}

	@AfterTest
	void close() {
		driver.close();
	}

}

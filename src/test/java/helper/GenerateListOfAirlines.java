package helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

public class GenerateListOfAirlines {
	
	public static void generateList(List<WebElement> flights,List<WebElement> price){
		Map<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();
	    
		 int size=flights.size();
	    for(int i=0;i<size;i++)
	    {
	    	ArrayList<Integer> str = new ArrayList<Integer>();
	    	if(map.containsKey(flights.get(i).getText())){
	    		str = map.get(flights.get(i).getText());
	    		int value = StringToInteger(price.get(i).getText());
	    	    str.add(value);
	    	}
	    	map.put(flights.get(i).getText(), str);
	    }
	    for(Map.Entry<String, ArrayList<Integer>> entry : map.entrySet()){
	    	ArrayList<Integer> a = entry.getValue();
	    	int min = Collections.min(a);
	    	int max = Collections.max(a);
	    	System.out.println("Airline Name- " + entry.getKey() + " Number of Flights- " + a.size() + " Minimum Fare- " + min + " Maximum Fare- " + max);
	    }
	}
	
	public static int StringToInteger(String s1){
		String[] s = s1.split(" ");
		String prices = s[1];
		int ans = 0;
		for(int i=0;i<prices.length();i++){
			if(prices.charAt(i)!=',')
				ans = ans*10 + prices.charAt(i)-'0';
		}
		return ans;
	}

}

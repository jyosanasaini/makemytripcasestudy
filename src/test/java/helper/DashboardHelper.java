package helper;

import org.openqa.selenium.WebDriver;

import pageObject.FlightDashboard;
import pageObject.FlightDetails;

public class DashboardHelper {

	WebDriver driver;

	public DashboardHelper(WebDriver driver) {
		this.driver = driver;
	}

	public void setDetails(String from, String to, String date) {
		FlightDashboard obj = new FlightDashboard(driver);
		obj.Click_on_Flight();
		obj.From_Location(from);
		obj.To_Location(to);
		obj.Departure_Date(date);
		obj.search();
	}

	public void searchFlights() {
		FlightDetails obj = new FlightDetails(driver);
		obj.countflight();
	}
}

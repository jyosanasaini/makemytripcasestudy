package pageObject;

import org.apache.commons.math3.stat.descriptive.SynchronizedMultivariateSummaryStatistics;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FlightDashboard {
	 WebDriver driver;
	
	 public FlightDashboard(WebDriver driver)
	 {
		 this.driver = driver; 
	 }
	
	public void Click_on_Flight(){
		driver.findElement(By.xpath(".//ul[@class='makeFlex font12']//li[1]")).click();
		driver.findElement(By.xpath(".//ul[@class='fswTabs latoBlack greyText']//li[1]")).click();

	}
	
	public void From_Location(String from) {
		
		driver.findElement(By.xpath("//div[@class='fsw_inputBox searchCity inactiveWidget ']")).click();
		driver.findElement(By.xpath(".//input[@placeholder='From']")).sendKeys(from);
		try{
		Thread.sleep(1000);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		driver.findElement(By.xpath(".//ul[@role='listbox']/li/div/div/p[contains(.,'"+from+"')]")).click();	
		
	}
	
	public void To_Location(String to) {
		
		driver.findElement(By.xpath(".//input[@placeholder='To']")).sendKeys(to);
		try{
			Thread.sleep(1000);
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		driver.findElement(By.xpath(".//ul[@role='listbox']/li/div/div/p[contains(.,'"+to+"')]")).click();
		
	}
	
	public void Departure_Date(String date){
		
		//driver.findElement(By.xpath("//label[@for='departure']")).click();
		driver.findElement(By.xpath("//div[@aria-label='" + date + "']")).click();
		
	}
	public void search(){
		driver.findElement(By.xpath("//a[@class='primaryBtn font24 latoBlack widgetSearchBtn ']")).click();

	}

}

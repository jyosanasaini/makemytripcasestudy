package pageObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.GenerateListOfAirlines;

public class FlightDetails {

	WebDriver driver;

	public FlightDetails(WebDriver driver) {
		this.driver = driver;
	}

	public void countflight() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			long lastHeight = (Long) js.executeScript("return document.body.scrollHeight");

			while (true) {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
				Thread.sleep(2000);

				long newHeight = (Long) js.executeScript("return document.body.scrollHeight");
				if (newHeight == lastHeight) {
					break;
				}
				lastHeight = newHeight;
			}
			List<WebElement> flights = driver.findElements(By.xpath(".//span[@class='airways-name ']"));
			List<WebElement> price = driver.findElements(By.xpath(".//span[@class='actual-price']"));

			int size = flights.size();
			System.out.println("Number of Flights available " + size);
			GenerateListOfAirlines.generateList(flights, price);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
